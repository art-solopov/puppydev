_SIDEBAR_ICONS = {
    'about': 'smile-o'
}

def sidebar_icon(item):
    try:
        fa_class = _SIDEBAR_ICONS[item.slug]
        return "<span><i class=\"fa fa-{0}\"></i></span>".format(fa_class)
    except KeyError:
        return None

