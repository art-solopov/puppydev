Title: Newbie impressions: React
Date: 2017-01-10 18:19
Tags: Programming; JavaScript
Category: Impressions

Welcome, dear readers, to the first issue of *Newbie Impressions*, a
biased series where I look at various programming-related tools I'm
unfamiliar with and relate my experience with them.

As is always with such series, mileages vary, impressions differ and a
perspective of a seasoned developer would probably be more valuable
than mine. I hope to give you insights into experience of someone just
starting with the tool, probably even with a whole area of expertise.

This is more or less the case with today's post. I'm not a complete
newbie at front-end Javascript, but so far my experiences were more or
less limited to jQuery and some template engines. I played a bit with
Backbone and Backbone.Marionette, but those libraries don't seem as
"thorough" when it comes to views. In my opinion, jQuery is little
more than a convenient wrapper around various vanillaJS APIs.
Backbone is very useful when you have a REST backend to support it,
but its views are basically just glue between models and templates
(sure, it beats writing this glue yourself but that's not a lot to
say). Backbone.Marionette is a useful collection of Backbone
boilerplate... And that's it. It's Backbone, just with a little more
pre-written convenience views.

[React](https://facebook.github.io/react/) seems very different. It
cares not for fetching data or routing. Instead, it heavily focuses on
transforming supplied data into a DOM tree and managing said tree,
updating it when the data changes. Even managing events seems to take
a back seat to this task, with React just being content with passing
functions as handlers (not unlike ye olde pre-jQuery HTML when the
most common way to attach the handler was writing something like
`<button onclick="...">...`). And these functions do a very simple
thing: they *update data*. And when the data is updated, the component
is re-rendered.

I think this is the feature that makes React so attractive. At its
roots, it's *simple*. It's not like Ember or Angular, not some huge
framework with an all-encompassing set of features. It's a library
capable enough, I think, to fit a lot of projects and use cases, yet
small enough to fit a working "Hello world" on four lines
(from [React docs][1]):

```
ReactDOM.render(
  <h1>Hello, world!</h1>,
  document.getElementById('root')
);
```

No library is perfect, of course, and React is no exception. In my
opinion, it suffers from two issues, but neither of them are major.

The first issue I have is JSX. It seems to be a point of debate for
many people, and I can't say I'm fond of it myself. I understand that
it's probably a good metaphor for DOM, but frankly the inclusion of
this strange HTML-like-but-not-really syntax in JS files is
disorienting, not to mention that it requires some extra configuration
to make text editors treat it properly. The only reason React
gets away with this is because there's an official Babel plugin
for JSX, and I like using Babel in my own pet projects.

And yes, I know that you can technically work without it. But the
syntax is quite clunky and verbose, clearly designed to be just
understandable on a transpiled output and not used in the original
source.

The second issue (which is more like a nitpick) I have is that React's
flexibility means you have a lot of ways to write ugly code, and the
best practices seem to be "word of man", not "word of God". It seems
like the best practices are using the stateful components scarcely,
preferably keeping the state in one place, using more function-based
components... Or at least, that's what I found out. Maybe there's
more, maybe I'm wrong. Who knows.

But, like I said, neither of those issues are major. Overall, for me
React was quite fun with its combination of simplicity and power. You
really do feel capable of building anything with it.

If you want to get started with React, I highly recommend to start
with the [official quick start guide][2]. Do get a book if you need
best practices and other stuff that's beyond basics, but not before
that.

Thank you very much for reading and I hope this article was useful!


[1]: https://facebook.github.io/react/docs/hello-world.html
[2]: https://facebook.github.io/react/docs/installation.html
