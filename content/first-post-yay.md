Title: First post! Yay!
Date: 2015-02-09 20:00

Today, February 8th 2015, is an official start for my new blog - completely self-developed by me!

It was quite a long way for me. =) Starting with Wordpress, moving to Jekyll and finally deciding to write a blog engine on my own.

So... I hope you'll have fun reading it! More coming soon!
