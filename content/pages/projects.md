Title: My projects
Status: hidden

* [Puppydev](https://github.com/art-solopov/puppydev) - this blog.
* [Billy Bones](https://github.com/art-solopov/billy-bones-flask) - A bill
  management system written in Flask. It's already kinda working right now!

Also there are lots of various discontinued projects, playground projects and
projects that are basically in the embryo stage right now.
