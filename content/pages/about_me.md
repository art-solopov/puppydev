Title: About me

Hi there. My name is Artemiy. I'm 24 years old. I live in
Moscow, Russia.

I currently work as a Rails programmer and I consider myself pretty
well-versed in Ruby. I also like Python and CoffeeScript. I'm eager to
learn new technologies and frameworks.

I also like sci-fi, fantasy, comic books (Green Lantern FTW!), video
games, cartoons, free/open-source software (I am a teeny tiny teensy
bit of FSF/OSS fanatic).

I like to chat with nice people in general, so please feel free to
drop by and have a chat!