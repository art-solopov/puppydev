Title: Wow... Haven't posted anything for a month.
Date: 2015-04-05 20:00
Tags: Programming; Ruby

To start off, I'm not dead. At least I think so. And I'd like to talk about
what happened to me during this month.

First of all, there was the final of my "trial period". If anyone doesn't
know, a trial period is the period after someone enters a new job. During this
time, basically, you can be fired for the lack of skill or competence. Mine
was supposed to end before the end of March, but I was given a last "super-task",
developing a whole subsystem on my own.

Second, I was working on this blog. Most importantly (for me), I developed a new
post authoring system for easier writing. Now creating or editing a post is as
easy as uploading a text file! =) I probably should create a repo for them as
well. Other than that, I switched the deployment from Capistrano to Mina and
tweaked with caching policies and Nginx configs.

Other than that, I went to Rome with my mom and looked into some interesting
languages, like [Erlang](http://www.erlang.org/), [Elixir](http://elixir-lang.org/)
and [Crystal](http://crystal-lang.org/).

Anyhow, I'll try to make the next D3.js tutorial next week! Hope you'll like it!
