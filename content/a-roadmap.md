Title: A Roadmap
Date: 2015-03-05 20:00
Tags: Programming; Ruby

Hello everyone!

Well, first thing's first. Sorry for the massive delay on the D3.js tutorials.
I hope to release the next one this weekend, although with the upcoming holiday
(the [International Women's Day](http://en.wikipedia.org/wiki/International_Women%27s_Day)
is pretty popular in Russia and is an official day off too) I might be a bit
too busy. In the meantime though, I'd like to rant on this very blog a bit.

The blog (the software it's running on) was hand-written by myself with lots of
love and, as you might've guessed, lots of stupid decisions. I'm a very
inexperienced web developer and the fact that this is the first project of mine
that has been deployed on a proper server somewhere on the Internet doesn't
exactly help.

So I'm planning to change some things around here. Most of them are just
maintenance-related stuff that you won't actually see... Although you might
feel them. =) I want to:

* Install and configure the monitoring software. I tried to work with
[Monit](http://mmonit.com/monit/), but it's apparently a lot better with
init-style processes (Nginx, PostgreSQL and such) than with RVM and Unicorn that
are not launched via init scripts. I'm thinking about configuring
[God.](http://godrb.com/) Looks like really cool software.
* Switch application servers. [Unicorn](http://unicorn.bogomips.org/) is
nice and all but it eats away quite a lot of memory. Between two workers and the
master they consume more than 50% of the available memory as we speak. I'm
thinking about [Puma](http://puma.io/) as the replacement. After all, my app
should be thread-safe, right, guys? Right?
* Think about switching ORMs. Right now I use ActiveRecord which seems like
overkill (and is known to consume quite a lot of RAM).
I thought of getting rid of an ORM altogether and using the PostgreSQL
driver but that means I'd need to implement my own pagination. Which might or
might not be cool. Maybe use [Sequel](http://sequel.jeremyevans.net/) instead?
* Reconfigure Nginx to serve gzipped assets.
* Reconfigure Nginx to serve images from the server itself (I'm using Imgur
right now. Shameful display!)
* Implement a better routine for uploading posts. I don't want it to be an
open web route (for obvious security reasons), but right now it's quite a lot of
semi-manual database work. Not cool.
* Maybe do some redesign. Like go for lighter colors. I dunno.

Thank you very much for bearing with me and please tell me what you think about all this!
Do you have any other advice on me keeping this blog? Anything you'd like me to do?
