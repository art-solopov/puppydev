Title: Puppydev 2.0-β released!
Date: 2017-04-30 01:30
Tags: Programming; This site; CSS; Design

Hello again my dear readers!

In this short post, I happily proclaim the next step in development of
this site:
the [new theme!](https://github.com/art-solopov/puppydev-theme) Built
with the latest and the greatest in modern CSS and JavaScript
technology (except the part which aren't the latest or the greatest…),
it aims to provide the full PuppyDev experience to all users on both
PC and mobile.

It's still rough on edges and lacks some important pages, but that's
why I'm calling it "beta" and not "release".

Hope you all like the new theme! Please leave a comment below this
post and tell me what you think.

Till we meet again!
