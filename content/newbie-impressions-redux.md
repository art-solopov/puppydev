Title: Newbie impressions: Redux
Date: 2017-02-14 00:00
Tags: Programming; JavaScript
Category: Impressions

Welcome, dear readers, to another issue of *Newbie Impressions*, where
I look at various programming-related tools I'm not familiar with and
give out my honest first impressions.

Today's post one may call a continuation
of [my previous post][1]
concerning React.JS. React and Redux seems to be quite a popular
combo on the Net.

But let's not get ahead of ourselves, shall we? I'd like to give just
a bit of background first.

If you've coded any React (or read
React.JS
[docs on state and lifecycle][2]),
you know that React is based upon describing your UI as a set of
*components*. Each component has immutable *properties* and mutable
*state*. When the state of a component is updated, it re-renders
itself. Eventually, people have started extracting the state
into a high-level component. It improves debugging, and also lets you control
the data flow easier.

But, of course, thanks to React's flexibility, you can have multiple
components on the same webpage. With it comes the problem of sharing
state between those components. I believe it was this problem that
prompted Facebook engineers to
create [Flux.](https://facebook.github.io/flux/) I must admit, I have
barely glanced at Flux, but it leaves an unnerving
impression. You need to write quite a lot of
boilerplate to use it. And that is exactly where Redux comes
helpful.

[Redux.js](http://redux.js.org/) describes itself as something that
"evolves the ideas of Flux, but avoids its complexity", and I'm
inclined to agree. Even though the library makes you jump through some
unfamiliar hoops, it feels easy to use, almost as easy as React
itself.

The core idea of Redux is the *store*. As with Flux, the Redux store
is the "single source of truth", meaning that all data in your views
exists as a slice of the store's *state*. Said state is usually an
immutable plain Javascript object. It is manipulated through
a *reducer*, which are simply a Javascript function. A reducer takes the
store's current state and an *action* (another plain Javascript
object) and returns the next state for the store.

When you want to modify a store in any way, you don't just change the
underlying object. You send an action to a *dispatcher*, which calls
the reducer, updates the store's state and notifies all subscribers
that it has changed. Between the dispatcher and the reducer you can
install *middlewares*, special functions that can perform logging or
change the way your actions affect the store.

If it sounds simple, it's because, well, it is. Just like React, you
can whip up a simple Hello World in Redux in a matter of seconds. And
yet its modular design makes it immensely powerful.

Of course, like I said, there is some jumping through hoops. You need
to understand the concept of single store. And as your domain grows,
you'll need to learn the ways to split up your reducer into smaller
functions and gather them together later. Even Dan Abramov, the author
of Redux, wrote an [article][3] stating that it might be too
complicated for your project.

However, I do urge everyone working with front-end Javascript to try
it, especially so if you're working with smaller libraries like
React. Just go to the [Redux website,](http://redux.js.org/) write
some code and see for yourself.

I hope this article was useful. Thank you very much for reading!

[1]: {filename}newbie-impressions-react.md
[2]: https://facebook.github.io/react/docs/state-and-lifecycle.html
[3]: https://medium.com/@dan_abramov/you-might-not-need-redux-be46360cf367
