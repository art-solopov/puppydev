Title: My love letter to free software
Date: 2016-02-14 20:00
Tags: Programming; Free software

This post is dedicated to the [I ❤ Free Software
campaign.](https://fsfe.org/campaigns/ilovefs/2016/index.en.html)

*Disclaimer:* this post is going to be pretty link-heavy. I really want to give
proper credit and love to all the free software I've been using and am
currently using.

My story with free software is neither long, nor epic. I'm not a Linux guru, I
haven't been using free software exclusively since middle school, and
recompiling the kernel is still some ancient magic to me. Yet I still want to
share my story and express my love and gratitude for it.

In fact, I've barely used any free software until University. I had a PC with
good ol' Windows and good ol' MS Office. And the dial-up connection wasn't good
for anything other than quickly getting some info for a school report. The only
open source thing I can remember from these days are
[Firefox,](https://www.mozilla.org/firefox/) my favourite web browser to date.

The interesting part happened when I got into the University. The Internet got
cheap, I discovered a world of torrents (I had my fair share of pirated
software, and I'm not proud about it). I think it was the second year in the
Uni when my friend (who was an avid Linux user) suggested trying
[Ubuntu](http://www.ubuntu.com/) to me. I did, and I was blown away. I don't
even quite remember why I was so amazed at it. One way or the other, the "Unix
way" settled with me quickly.

As time went by, I worked more and more within Linux, eventually using
[VirtualBox](https://www.virtualbox.org/) for most of my Windows-related tasks.
MS Office was quickly ditched for [OpenOffice](http://www.openoffice.org/) and
then [LibreOffice.](https://www.libreoffice.org/) Around the third year I
discovered the power of [LaTeX:](https://latex-project.org/) a publishing tool
that greatly helped me prepare my reports. For our C++ programming courses I
went from Borland IDEs to [Code::Blocks](http://www.codeblocks.org/) and [Qt
Creator.](http://www.qt.io/developers/)

C++ wasn't my only interest though, and later I went to learning Java. My IDE
of choice was [NetBeans](https://netbeans.org/) (I didn't try
[Eclipse](http://www.eclipse.org/) until much later, and truth to be told, I
don't like it very much). However, later my attention was captured by Ruby and
Python. I tried a lot of IDEs for both of them (even using
[Spyder](https://github.com/spyder-ide/spyder) for quite some time) before I
decided to ditch them altogether in favour of simpler text editors.

This was a journey of its own. I tried Sublime Text, and I thought it was good,
but the "eternal trial" thing discouraged me. I decided to try something more
free. After several years I still struggle and oscillate between three editors:
[Vim](http://www.vim.org/) (and recently its spawn,
[NeoVim](https://neovim.io/)), [Emacs](https://www.gnu.org/software/emacs/) and
[Atom.](https://atom.io/) Atom is the new kid on the block who isn't as
experienced or skilled as the other two, but definitely has great potential.

In the end, my job and hobbies are tightly connected with using free software.
I work as a [Ruby on Rails](http://rubyonrails.org/) programmer, and currently
I'm trying to stick my nose into [Django](https://www.djangoproject.com/) and
[Node.JS,](https://nodejs.org/en/) and I've been using
[PostgreSQL](http://www.postgresql.org/) for years now.

I admit it, my contribution to open source is miserable at best, but as an avid
free software user, I just want to thank the people and the organisations
behind the movement and the projects. What you've done is awesome. I love free
software. I love how open it is. I love the fact that I can generally look at
the sources to learn and understand. I certainly love that, more often then
not, any issues I reported were addressed quickly and professionally.

I love all of it.

## Addendum

I realise that the software I listed previously is but a fraction of all the
free software I use and admire. A lot of it was sacrificed to the gods of
narrative. However, I'd still like to give credit where credit is due, so
here's a short list of free software that didn't make it into the main story:

* [KDE](https://www.kde.org/) and [Kubuntu;](http://kubuntu.org/)
* [Linux Mint,](http://www.linuxmint.com/) [Arch
  Linux](https://www.archlinux.org/) and [OpenSUSE;](https://www.opensuse.org/)
* [SQLite,](http://sqlite.org/) [ArangoDB](https://www.arangodb.com/) and
  [CouchDB;](http://couchdb.apache.org/)
* [Nginx;](https://www.nginx.com/)
* [Thunderbird;](https://www.mozilla.org/ru/thunderbird/)
* [Psi](http://psi-im.org/) and [Pidgin;](https://pidgin.im/)
* And many, many more that I currently can't remember.

Again, I'd like to thank the people and the organisations behind the projects.
