Title: More random thoughts
Date: 2015-05-04 20:00
Tags: Free software

Hello dear readers! Since the next tutorial is not ready (and, I'll be honest,
isn't anywhere near ready), I've decided to assemble my short notes on various
subjects.

Since the post is bound to be pretty long and disorderly, here's a table
of contents:

* [Grooveshark shutdown](#grooveshark-shutdown)
* [Emacs](#emacs)
* [Improving the blog](#improving-the-blog)

## Grooveshark shutdown

As you might now, a musical service named Grooveshark was [shut down][grooveshark_shutdown]
by the court's decision on April 30th. The reason for the shutdown was
Grooveshark's lack of music licenses.

[grooveshark_shutdown]: http://www.theguardian.com/technology/2015/may/01/grooveshark-shuts-down-labels-music

Now, what was Grooveshark? It was a service that provided  on-demand access
to thousands of songs of various genres, labels and artists. It was free
on desktop, although I know that they did have a subscription service called
"Grooveshark Anywhere" that let you use the same on-demand service on mobile.
The music was uploaded by users, so the site featured even more obscure artists.

There are quite a lot alternatives, mostly suggested in [this Reddit thread.][grooveshark_alts]
I've been using [Yandex.Music][yandex_music] for quite some time and I
decided to try [Plug.dj][plug_dj] and [Deezer][deezer] recently. Both have
their advantages and disadvantages.

[grooveshark_alts]: http://www.reddit.com/r/grooveshark/comments/34h1dl/alternatives_to_grooveshark_thread/
[yandex_music]: https://music.yandex.ru/
[plug_dj]: https://plug.dj/
[deezer]: http://www.deezer.com/

Now, you could easily blame the people who ran Grooveshark for the shutdown.
That's the point I've been seeing lately on TheRegister: they haven't been
playing by the rules and they got what they deserved. Personally, I think
the rules have been changed (and still are being changed) to appeal to a
very specific (and rich) group.

Anyway, copyright system issues is a topic for another day and another post.
In the meantime, I heartily recommend you to read Lawrence Lessig's *Free
Culture*. It can be downloaded for free [here.][lessig]

[lessig]: http://free-culture.cc/freecontent/

## Emacs

I've been using Vim for the last... Year or so? Yeah, looks like it. Well,
recently I decided to start learning Emacs.

I was lured in by the arguments of more solid codebase. Plus, Lisp was
considered a better configuration/extension language than VimScript.

Now, I’ve had some frustrating time with Common Lisp. Although I do
blame the book I was reading for trying to introduce macros far too early
and not explaining them properly. Naturally, I was vary.

At first, I spent the majority of time banging my head against a wall.
Which *was* probably my fault, but c'mon, *reading* manuals like books is hard!
Now I'm pleased to see I'm being quite productive with Emacs. It's amazing
how much it's "batteries included", featuring even its own package manager and
configuration system. And Lisp, while probably not being a particularly
good language for creating a large application, is pretty good for
configuration and extension. It lets you pass functions and variables around
quite easily.

Similarly to Vim, I decided to version-control my Emacs configuration on
GitHub, resulting in the [emacsd][emacsd] repo.

[emacsd]: https://github.com/art-solopov/emacsd

## Improving the blog

Recently I made some changes to this blog again, although it wasn't one
instantly visible. I decided to switch from [Sprockets][sprockets] to [Gulp][gulp]
for managing my assets. I think I'll write about my thoughts of this transition
someday later. There are some more improvements I'd like to make though:

* Javascript. I already use it for Disqus comments, but so far I've not
written a `<noscript>` placeholder for it or use it anywhere else. The
fact is, I'm not particularly happy with the current code highlighter
I've been using, [CodeRay,][coderay] and since I'm using JavaScript anyway,
using [Highlight.js][highlightjs] shouldn't be a problem. I also want to
introduce a site-wide page of what I'm using Javascript for and link it
in my `<noscript>` tag.
* Color scheme. People were telling me that the colorscheme I'm using is
not particularly comfortable on the eyes, so I'm looking for inspiration
for my next one!
* Fulltext search. I'm too lazy to install something like ElasticSearch
or something, so I'll try using the [PostgreSQL][postgresql_fulltext]
facilities. Hope it'll work well with Markdown!

[sprockets]: https://github.com/rails/sprockets
[gulp]: http://gulpjs.com/
[coderay]: http://coderay.rubychan.de/
[highlightjs]: https://highlightjs.org/
[postgresql_fulltext]: http://www.postgresql.org/docs/current/static/textsearch.html

Thank you very much for the reading and please circulate the links!
