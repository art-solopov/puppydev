Title: Random thoughts
Date: 2015-07-02 20:00
Tags: Politics; Programming

Hello, dear readers.

Wow. I haven't written in a while, have I? My latest blog post dates as far as
4th of May, which makes it almost 2 months already.

So I've collected some of random unrelated thoughts. Enjoy!

---

So, why haven't I been writing for so long?
Now, there are several reasons for this. First of all, I didn't feel like I've
had lots of stuff to write about. I don't know if I should write any really
short posts or not, but for now I'm trying to write at least several paragraphs.

There's also the problem with making my blog posts, well, appear on the site
itself. As of now I use a mix of Markdown and YAML to include both text and
metadata in one file.  If anyone knows an app or a library that would
let me quickly write and prepare my posts without requiring a web interface
(because I really don't wanna set up authentication or HTTPS on the server),
I'd be quite happy to learn about it.

---

Everybody's talking about the American's Supreme Court desicion to support
same-sex marriages. First and foremost, I fully support this decision and
celebrate it as a step forward towards equality and human rights in general.
It does have that smell of populism, but hey, it's politicians, they *never*
do anything for anyone but themselves, so it's our win regardless of
motivation.

And before anyone asks, no, I'm not homosexual. I just think that people have
all rights to love each other and that marriage is a useful tool sometimes.

---

Had my wisdom tooth removed this week, because it started to ache suddenly. In
the middle of a night. Just... Add toothache to a list of stuff I wish even my
greatest enemy wouldn't experience.

The operation itself was fine though, and I'm recovering pretty well. Not even
opened the painkillers yet!

---

Frankly, I'm thinking about abandoning Bootstrap for this blog.
Really, I only use it for relatively minor things, like grids and some styling
(buttons, list, etc), which is very nice, but probably does not justify the use of a
relatively big front-end framework that can do stuff like modals, spyscrolling,
dropdowns etc. So now I'm thinking to gravitating towards stuff like [Bourbon](http://bourbon.io/)
and [Neat,](http://neat.bourbon.io/) with coding my own styles (or just copying
them from Bootstrap, that works too). Not all Bourbon mixins seem useful to me,
of course, but some of them are pretty interesting!

---

And that's all for today folks! Thank you for reading! Leave your opinions and
impressions in the comments and circulate the links!
