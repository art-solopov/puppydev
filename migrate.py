#!/usr/bin/env python
import os, os.path, sys, re, io
from datetime import timedelta
from dateutil import parser as dateparser
from glob import glob
from yaml import load
from pelican.utils import slugify

def convert(path):
    print("Converting {0}".format(path))
    fc = open(path).read()
    fm, content = re.split('\s*===\s*', fc, flags=re.MULTILINE)
    front_matter = load(io.StringIO(fm))

    date = re.match('\d{4}-\d{2}-\d{2}', os.path.basename(path)).group(0)
    date = dateparser.parse(date)
    date += timedelta(hours=20)

    content = re.sub("\s*\[\s*\]\(#cut\)\s*", "\n\n", content, flags=re.MULTILINE)

    out_filename = slugify(front_matter['title']) + '.md'
    out_path = os.path.join('content', out_filename)
    if os.path.exists(out_path):
        out_path = out_path.replace('.md', '-0.md')

    with open(out_path, 'w') as outf:
        outf.write("Title: " + front_matter['title'] + "\n")
        outf.write("Date: " + date.strftime('%F %H:%M') + "\n")
        outf.write("Tags: " + '; '.join(front_matter['tags']) + "\n")
        outf.write("\n")
        outf.write(content)

if __name__ == '__main__':
    directory = sys.argv[1]
    print(directory)
    for p in glob(os.path.join(directory, '*')):
        if p.endswith('.md'):
            convert(p)
