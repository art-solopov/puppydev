#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

import sys
from os import environ as env
from os.path import join as pjoin
sys.path.append('.')

import lib.filters as filters

AUTHOR = 'Artemiy Solopov'
SITENAME = 'Puppydev'
DEFAULT_DATE_FORMAT = '%Y-%m-%d'
DISPLAY_PAGES_ON_MENU = True
LOAD_CONTENT_CACHE = False
DELETE_OUTPUT_DIRECTORY = True

PATH = 'content'

TIMEZONE = 'Europe/Moscow'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
	# ('Pelican', 'http://getpelican.com/'),
 #    ('Python.org', 'http://python.org/'),
 #    ('Jinja2', 'http://jinja.pocoo.org/'),
)

# Social widget
SOCIAL = (
	('Twitter', 'https://twitter.com/ArtSolopov'),
	('GitHub', 'https://github.com/art-solopov')
)

DEFAULT_PAGINATION = 5
DEFAULT_CATEGORY = 'Miscellanious'

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

DIRECT_TEMPLATES = ['index', 'tags', 'categories', 'archives']
TAGS_SAVE_AS = 'tags/index.html'
CATEGORIES_SAVE_AS = 'categories/index.html'
ARCHIVES_SAVE_AS = 'archives.html'

ARTICLE_URL = '{date:%Y-%m-%d}/{slug}/'
ARTICLE_SAVE_AS = ARTICLE_URL + 'index.html'

PAGE_URL = '{slug}/'
PAGE_SAVE_AS = PAGE_URL + 'index.html'

CATEGORY_URL = 'category/{slug}/'
CATEGORY_SAVE_AS = CATEGORY_URL + 'index.html'

TAG_URL = 'tag/{slug}/'
TAG_SAVE_AS = TAG_URL + 'index.html'

AUTHOR_URL=''
AUTHOR_SAVE_AS = ''

# THEME = 'notmyidea'
THEME = 'puppydev-theme'

